var itemsFamille = null;
var itemsAmis = null;
var itemsCollaborateurs = null;
var itemsContemporains = null;
var itemsOtlet = null;
var itemsCollegues = null;
var itemsAutres = null;

var itemsPersonne = null;
var itemsInstitution = null;
var itemsEvenement = null;
var itemsOeuvre = null;

var showFamille = true;
var showAmis = true;
var showCollaborateurs = true;
var showContemporains = true;
var showOtlet = true;
var showCollegues = true;
var showAutres = true;

var showPersonne = true;
var showInstitution = true;
var showEvenement = true;
var showOeuvre = true;

function togglePersonne() {
if (showPersonne == true) {
	document.getElementById('buttonPersonne').className += " activebutton";
	itemsPersonne = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.type == "Personne";
		}
	});
	nodes.remove(itemsPersonne);
	console.log('filtered itemsPersonne', itemsPersonne);
	showPersonne = false;
	}
	else{
	nodes.add(itemsPersonne);
	showPersonne = true;
	document.getElementById('buttonPersonne').classList.remove("activebutton");
	}
}

function toggleInstitution() {
if (showInstitution == true) {
	document.getElementById('buttonInstitution').className += " activebutton";
	itemsInstitution = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.type == "Institution";
		}
	});
	nodes.remove(itemsInstitution);
	console.log('filtered itemsInstitution', itemsInstitution);
	showInstitution = false;
	}
	else{
	nodes.add(itemsInstitution);
	showInstitution = true;
	document.getElementById('buttonInstitution').classList.remove("activebutton");
	}
}

function toggleEvenement() {
if (showEvenement == true) {
	document.getElementById('buttonEvenement').className += " activebutton";
	itemsEvenement = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.type == "Évènement";
		}
	});
	nodes.remove(itemsEvenement);
	console.log('filtered itemsEvenement', itemsEvenement);
	showEvenement = false;
	}
	else{
	nodes.add(itemsEvenement);
	showEvenement = true;
	document.getElementById('buttonEvenement').classList.remove("activebutton");
	}
}

function toggleOeuvre() {
if (showOeuvre == true) {
	document.getElementById('buttonOeuvre').className += " activebutton";
	itemsOeuvre = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.type == "Œuvre";
		}
	});
	nodes.remove(itemsOeuvre);
	console.log('filtered itemsOeuvre', itemsOeuvre);
	showOeuvre = false;
	}
	else{
	nodes.add(itemsOeuvre);
	showOeuvre = true;
	document.getElementById('buttonOeuvre').classList.remove("activebutton");
	}
}

function toggleFamille() {
document.getElementById('buttonFamille').className += " activebutton";
if (showFamille == true) {
	itemsFamille = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.group == "famille";
		}
	});
	nodes.remove(itemsFamille);
	console.log('filtered itemsFamille', itemsFamille);
	showFamille = false;
	}
	else{
	nodes.add(itemsFamille);
	showFamille = true;
	document.getElementById('buttonFamille').classList.remove("activebutton");
	}
}

function toggleAmis() {
document.getElementById('buttonAmis').className += " activebutton";
if (showAmis == true) {
	itemsAmis = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.group == "ami";
		}
	});
	nodes.remove(itemsAmis);
	console.log('filtered itemsAmis', itemsAmis);
	showAmis = false;
	}
	else{
	nodes.add(itemsAmis);
	showAmis = true;
	document.getElementById('buttonAmis').classList.remove("activebutton");
	}
}

function toggleCollaborateurs() {
document.getElementById('buttonCollaborateurs').className += " activebutton";
if (showCollaborateurs == true) {
	itemsCollaborateurs = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.group == "collaborateur";
		}
	});
	nodes.remove(itemsCollaborateurs);
	console.log('filtered itemsCollaborateurs', itemsCollaborateurs);
	showCollaborateurs = false;
	}
	else{
	nodes.add(itemsCollaborateurs);
	showCollaborateurs = true;
	document.getElementById('buttonCollaborateurs').classList.remove("activebutton");
	}
}

function toggleCollegues() {
document.getElementById('buttonCollegues').className += " activebutton";
if (showCollegues == true) {
	itemsCollegues = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.group == "collegue";
		}
	});
	nodes.remove(itemsCollegues);
	console.log('filtered itemsCollegues', itemsCollegues);
	showCollegues = false;
	}
	else{
	nodes.add(itemsCollegues);
	showCollegues = true;
	document.getElementById('buttonCollegues').classList.remove("activebutton");
	}
}

function toggleContemporains() {
document.getElementById('buttonContemporains').className += " activebutton";
if (showContemporains == true) {
	itemsContemporains = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.group == "contemporain";
		}
	});
	nodes.remove(itemsContemporains);
	console.log('filtered itemsContemporains', itemsContemporains);
	showContemporains = false;
	}
	else{
	nodes.add(itemsContemporains);
	showContemporains = true;
	document.getElementById('buttonContemporains').classList.remove("activebutton");
	}
}

function toggleAutres() {
document.getElementById('buttonAutres').className += " activebutton";
if (showAutres == true) {
	itemsAutres = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.group == "autre";
		}
	});
	nodes.remove(itemsAutres);
	console.log('filtered itemsAutres', itemsAutres);
	showAutres = false;
	}
	else{
	nodes.add(itemsAutres);
	showAutres = true;
	document.getElementById('buttonAutres').classList.remove("activebutton");
	}
}

function toggleOtlet() {
document.getElementById('buttonOtlet').className += " activebutton";
if (showOtlet == true) {
	itemsOtlet = nodes.get({
		// fields: ['id'],
		filter: function(item) {
			return item.group == "otlet";
		}
	});
	nodes.remove(itemsOtlet);
	console.log('filtered itemsOtlet', itemsOtlet);
	showOtlet = false;
	}
	else{
	nodes.add(itemsOtlet);
	showOtlet = true;
	document.getElementById('buttonOtlet').classList.remove("activebutton");
	}
}