# Otletosphère v2

Les données du graphe sont contenues dans ces 2 tableurs Google Sheets :

- Nœuds : <https://docs.google.com/spreadsheets/d/14LrnBPEIrDRoabhxiMZInI5ADePk2H0Aiylt50Lo0oE/edit?usp=sharing>
- Liens : <https://docs.google.com/spreadsheets/d/1ALek0LR-M8GQ2DrCych6a4W1WI6a_mrf8hpkMyCqmTY/edit?usp=sharing>

## Objectifs

- redéfinir les entités
- redéfinir les liens entre les entités et Otlet, les liens entre entités
- redéfinir la représentation graphique des liens et des entités
- ré-alimenter progressivement la base de données
- ajustements graphiques (formes des entités, couleurs des liens)
- refonte du mécanisme de manipulation des données (actuellement : Tabletop.js)

Entités prévues : personne, organisation, évènement, œuvre (productions intellectuelles au sens de _work_ dans le modèle FRBR, focus sur les œuvres à dimension relationnelle càd qui permettent d'établir des liens).

## Consignes pour alimenter la base

- pas d'accents dans les noms de fichier image
- les liens sont colorés en fonction du nœud de départ (« _from_ »)

## Legacy

Fichiers v1 : <https://drive.google.com/file/d/1zR3spIvFqxeIvgMcFo89CIydVYRD5Qk6/view?usp=drive_web>  
Spreadsheet v1 : <https://docs.google.com/spreadsheets/d/1zu-Hs1fp9Lu5TCGTgzQgwF62BnADHYKesyygZNunDFM/edit?usp=sharing>